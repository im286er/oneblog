<?php

namespace Admin\Controller;
use Think\Controller\RestController;
use Think\Model as MODEL;

class ApiController extends RestController{

	protected $defaultType = 'json';

    public function _initialize(){
    	if(!defined('IS_AJAX'))
			define('IS_AJAX', 1);
		C('SHOW_PAGE_TRACE', false);
		header('Access-Control-Allow-Origin:*');
    }

	public function _empty(){
		if(method_exists($this,str_replace('.', "_{$this->_method}_", ACTION_NAME))){
			$fun = str_replace('.', "_{$this->_method}_", ACTION_NAME);
		}else if(method_exists($this, str_replace('.', '_', ACTION_NAME))){
			$fun = str_replace('.', '_', ACTION_NAME);
		}
		if(method_exists($this, $fun))
			$this->$fun();
		else
			$this->response('',$this->type, 404);
	}

	// 查询
	public function get_json($id=0, $model, $single = 0){
		$table = D($model);
		if(!$table)
			$this->response('无法获取数据','json', 404);
		if($id)
			$map[$table->pk] = $id;
		$res = $single ? $table->where($map)->find(): $table->where($map)->select();
		if($res)
			$this->succ('获取成功', array('list'=>$res));
		else
			$this->succ('无法获取数据');
	}

	//新增
	public function post_json(){
		$table = D(I('post.model'));
		if(!$table)
			$this->err('错误的对象');
		if(!$table->create())
			$this->err($table->getError());
		else
			if($table->add())
				$this->succ('添加成功',$table->create());
			else
				$this->err('添加失败');
	}

	// 更新
	public function put_json($model){
		$table = D($model);
		if(!$table)
			$this->succ('错误的对象');
		if(!$table->create(I('put.')))
			$this->err($table->getError());
		if($table->save($table->create(I('put.'))) !== false)
			@$this->succ('更新成功');
		else
			@$this->err('更新失败');
	}

	// 删除
	public function delete_json($model){
		$table = D(I('delete.model'));
		if(!$table)
			$this->response('错误的对象', 'json', 404);
		$id = I('delete.id');
		if(is_array($id)){
			$count = count($id);
			$table->startTrans();
			$res = $table->where(array($table->pk=>array('in', $id)))->delete();
			if($res == $count){
				$this->response('删除成功', 'json', 202);
			}else{
				$table->rollback();
				$this->response('部分删除失败，已回滚', 'delete', 206);
			}
		}else{
			$res = $table->where(array($table->pk=>$id))->delete();
			if($res == 1){
				$this->response('删除成功', 'json', 202);
			}else{
				$this->response('删除失败',  'json', 500);
			}
		}
	}

	protected function succ($message, $data = array()){
		$odata = array('info'=>$message,'status'=>1);
		if(is_array($data) && $data !== array()){
			$odata = array_merge($odata, $data);
		}
		$this->response($odata, 'json', 200);
	}

	protected function err($message, $data = array()){
		$odata = array('info'=>$message,'status'=>0);
		if(is_array($data) && ($data !== array())){
			$odata = array_merge($odata, $data);
		}
		$this->response($odata, 'json', 200);
	}

	public function get_single(){
		$list = M('Article')->where("type=2")->select();
		$this->succ('',array());
	}

	public function get_cate(){
		$nav_tree = D('Cate')->select();
        $nav_tree = D('Tree')->toFormatTree($nav_tree);
		$this->succ('',$nav_tree);
	}

	public function get_article(){
		$list = M('Article')->where("type=1")->select();
		$this->succ('',$list);
	}

}
