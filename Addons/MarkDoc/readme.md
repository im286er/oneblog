## 功能
本工具用于通过一个存放Markdown笔记的目录生成笔记展示页。

![展示](http://www.onethink.cn/Uploads/Editor/2014-07-02/53b3706ba7702.png)

通过插件配置可以定制显示。

![配置](http://www.onethink.cn/Uploads/Editor/2014-07-02/53b370dd9dc3e.png)

[配置参考](https://github.com/xin-meng/daux)

>推荐大家考虑中国的国情，谷歌分析和推特不要配置，拖慢浏览者访问速度。

## 文件

生成器将会搜索/docs目录及其子目录中的Markdown *.md 文件. 译者注:Markdown是一种轻量级的标记语言.

你必须使用.md 文件扩展名命名你的文档文件. 通过目录和文件名带编号能实现显示时的自然排序

比如  01 aa/01.a.md 01 aa/02.b.md

## 图片

文档中图片位于目录docs（默认这个，可以配置）下面的images下，在markdown.md中引入内部图片只需要 写`![类与对象原理图](images/1.png)` 这样的就行了。

安装好后自带了 老朱的笔记 算是福利，可以选择删除。保留index.md 

插件的安装后使用，参考我[OneBlog博客的二次开发](http://www.topthink.com/topic/1340.html)。插件的安装后使用，参考我[OneBlog博客的二次开发](http://www.topthink.com/topic/1340.html)。

## TODO 
* 代码高亮优化成Sublime中mono的风格

## 反馈
大家可以在极思维 [OneBlog 小组上](http://www.topthink.com/topic/1403.html)讨论。也可以在[git](http://git.oschina.net/yangweijie/oneblog)上写issue。
