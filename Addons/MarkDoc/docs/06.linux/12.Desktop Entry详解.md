Desktop Entry 文件标准简介
===========================

在 Windows 平台上，用户可以通过点击位于桌面或菜单上的快捷方式轻松打开目标应用程序。现代 Linux 桌面系统也提供了此项功能。目前，Linux KDE 和 Linux GNOME 桌面系统都使用 Desktop Entry 文件标准来描述程序启动配置信息。Desktop Entry 文件标准是由 [FreeDesktop.org](http://freedesktop.org/wiki/) 制定的，目前最新的版本是"Desktop Entry Specification 1.0"。

Desktop Entry 文件结构
========================

Desktop Entry 文件以 .desktop作为后缀名，例如：gnome的应用程序快捷方式存放在 `/usr/share/applications`目录下。如果需要在桌面启动某些应用程序，只需从这里复制相应的快捷方式到桌面上。

Desktop Entry 文件通常以字符串"[Desktop Entry]"开始，而内容是由若干 `关键字=数值` 配对的 Entry 组成

标准规范定义文件在：<http://standards.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html>,以下列出常用的一些关键字


|Key           |Description                                                                                                                                                                                              |值的类型    |必选    |Type    |
|--------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------|--------|--------|
|Type          |三种类型 Application , Link , Directory                                                                                                                                                                  |string      |YES     |-       |
|Name          |定义菜单显示的名称                                                                                                                                                                                       |localestring|YES     |1-3     |
|NoDisplay     |定义该菜单不显示                                                                                                                                                                                         |boolean     |NO      |1-3     |
|Comment       |菜单的Tooltip                                                                                                                                                                                            |localestring|NO      |1-3     |
|Icon          |Icon图标.可以使用图标文件的绝对路径，或者Icon Theme定义的名称。                                                                                                                                          |localestring|NO      |1-3     |
|Exec          |执行的命令，可以使用参数，只有Type是Application时才有效                                                                                                                                                  |string      |YES     |1       |
|Path          |定义Application运行时的工作目录                                                                                                                                                                          |string      |NO      |1       |
|Terminal      |是否在命令行中运行                                                                                                                                                                                       |boolean     |NO      |1       |
|Actions       |命令分组.见下文                                                                                                                                                                                          |string(s)   |NO      |1       |
|MimeType      |定义应用生效的mime类型                                                                                                                                                                                   |string(s)   |NO      |1       |
|Categories    |菜单出现在哪个分类中，见(见下文)                                                                                                                                                                         |string(s)   |NO      |1       |
|StartupNotify |是否在运行前进行提示确认。                                                                                                                                                                               |boolean     |NO      |1       |
|URL           |只有在Type是Link时才有效,指向的目标                                                                                                                                                                      |string      |YES     |2       |


Exec 关键字
------------

命令支持参数，如果参数含有命令行保留字符(例如空格,分号等)必须用引号包围.


Reserved characters are space (" "), tab, newline, double quote, single quote ("'"), backslash character ("\"), greater-than sign (">"), less-than sign ("<"), tilde ("~"), vertical bar ("|"), ampersand ("&"), semicolon (";"), dollar sign ("$"), asterisk ("*"), question mark ("?"), hash mark ("#"), parenthesis ("(") and (")") and backtick character ("`").


#### 动态参数

对于选择文件或目录后的右键菜单，Exec支持动态参数


|参数|含义                                                                                      |
|----|------------------------------------------------------------------------------------------|
|%f  |单个文件名，如果选择了多个文件，则每个文件名都各自作为参数单独执行命令                    |
|%F  |多个文件名，如果程序支持多个文件名作为参数使用这个动态参数，所有文件一次传给命令进行执行。|
|%u  |单个url                                                                                   |
|%U  |多个url                                                                                   |





Action 关键字
---------------

它用来定义多个分组标识，多个标识使用分号隔开 group。 然后在后面继续定义具体的action区块 [Desktop Action %s]   %s是前面定义的标识之一，action区块的定义支持 Name,Icon,Exec关键字


桌面程序菜单定义
------------------

主菜单分类定义文件是一个xml文件，位于 `$XDG_CONFIG_DIRS/menus/${XDG_MENU_PREFIX}applications.menu`


Icon Theme定义
------------

一个 Icon Theme 就是一组风格相同的图标文件，支持PNG,SVG,XPM文件，位于`$XDG_DATA_DIRS/icons`目录,每个主题至少有一个  index.theme 文件描述主题，可能还有cursor.theme描述鼠标指针。具体的主题定义规范见：<http://standards.freedesktop.org/icon-theme-spec/icon-theme-spec-latest.html>


扩展Desktop Entry
===============

KDE Desktop Entry
------------------

增加了一些关键字

Type   增加了ServiceType, Service and FSDevice


对于Type为FSDevice的类型，增加了下面几种关键字


|Key        |Description                                                                                |Value Type|
|-----------|-------------------------------------------------------------------------------------------|----------|
|Dev        |The device to mount.                                                                       |string    |
|FSType     |The type of file system to try to mount.                                                   |string    |
|MountPoint |The mount point of the device in question.                                                 |string    |
|ReadOnly   |Specifies whether or not the device is read only.                                          |boolean   |
|UnmountIcon|Icon to display when device is not mounted. Mounted devices display icon from the Icon key.|string    |




对于Type为 ServiceType, Service的desktop文件存放于`/usr/share/kde4/servicetypes`和`/usr/share/kde4/services`
