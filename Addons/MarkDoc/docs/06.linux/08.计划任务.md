crontab
=======

权限配置文件
------------

> 用来设置允许执行`cron`命令的用户

> `/etc/cron.allow`与`/etc/cron.deny`

* `/etc/cron.allow`优先
* 一个帐号一行
* 系统默认是仅含有 `/etc/cron.deny` 文件 

日志文件
--------

> /var/log/cron

如果你的 Linux 不知道有否被植入木马时，也可以搜寻一下 /var/log/cron 这个日志文档

任务配置文件
----------

> /etc/crontab

```
[root@www ~]# cat /etc/crontab

# 使用哪种 shell 介面
SHELL=/bin/bash 
# 运行档搜寻路径
PATH=/sbin:/bin:/usr/sbin:/usr/bin 
# 若有额外STDOUT，以 email将数据送给谁
MAILTO=root 
# 默认此 shell 的家目录所在
HOME=/ 

# 计划任务配置共七列
# 分   时   日   月 星期  以谁的身份执行	 执行的命令
  17   *     *   *   *    root            cd / && run-parts --report /etc/cron.hourly
  25   6     *   *   *    root            test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.daily )
  47   6     *   *   7    root            test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.weekly )
  52   6     1   *   *    root            test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.monthly )
```
 
crontab 语法
------------

> 当普通用户执行这个命令时,设置的计划任务的执行身份即是用户自己,无法改变

```
[root@www ~]# crontab [-u username] [-l|-e|-r]

选项与参数：
-u ：只有 root 才能使用这个选项，亦即帮其他使用者创建/移除 crontab 工作排程；
-e ：编辑 crontab 的工作内容
-l ：查阅 crontab 的工作内容
-r ：移除所有的 crontab 的工作内容，若仅要移除一项，请用 -e 去编辑。
```

|列      | 分钟| 小时 |日期   |月   |周     |
|--------|-----|------|-------|-----|-------|
|取值范围|0-59 |0-23  |1-31   |1-12 |0-7    |

{r:注意：周与日月不能同时设置！}

|特殊字符|含义     |
|--------|---------|
|*       |任意时间 |
|,       |多分隔个时间段。 `0 3,6 * * * command` 代表 3 点与 6点 都适用！|
|-       |时间范围。 `20 8-12 * * * command`|
|/n	     |那个 n 代表数字，是『每隔 n 单位间隔』的意思，例如每五分钟进行一次，则：`*/5 * * * * command`|

### 范例一：

每天的 12:00 发信给自己

```
[dmtsai@www ~]$ crontab -e

# 此时会进入 vi 的编辑画面让您编辑工作！注意到，每项工作都是一行。
0   12  *  *  *  mail dmtsai -s "at 12:00" < /home/dmtsai/.bashrc
# 你会发现配置里列只有6列,没有user列,因为普通用户不能使用user列
```

一些注意事项
------------

1. 资源分配不均的问题

    * 举例来说，如果我们有四个工作都是五分钟要进行一次的，如果同时进行，
    该四个动作又很耗系统资源！此时就需要设置好详细而且具体的时间取代模糊的每5分钟。例如下面这样设置：

    ```
    [root@www ~]# vi /etc/crontab
    1,6,11,16,21,26,31,36,41,46,51,56 * * * * root CMD1
    2,7,12,17,22,27,32,37,42,47,52,57 * * * * root CMD2
    3,8,13,18,23,28,33,38,43,48,53,58 * * * * root CMD3
    4,9,14,19,24,29,34,39,44,49,54,59 * * * * root CMD4
    ```

2. 取消不要的输出项目

    * 由于当有运行成果或者是运行的项目中有输出的数据时，该数据将会 mail 给 MAILTO 配置的帐号。
    所以对于某些重复出现或者并不必要的输出可以将输出的结果重定向输出到 /dev/null

anacron
=======

> 除非我们的机器保持每天都24小时开始，否则就会有些系统例行工作都没有人做了，这个时候就可以用到anacron了

> 所以 anacron 并不能指定何时执行某项任务，而是以天为单位或者是在开机后立刻进行 anacron 的动作，
他会去侦测停机期间应该进行但是并没有进行的 cron服务，如果有就将该任务执行一遍，然后就自动停止。

> anacron是一个命令,不是一个服务,所以他要配置在crontab中才有有效果,在最上面的crontab示例中你已经看到了anacron命令的存在;它一般也不需要自己配置额外的设定,使用预设值执行即可.

如果要检查anacron是否会在开机时自动执行,centos可以执行`chkconfig --list anacron`

配置文件
---------

cat /etc/anacrontab


at
==

> at 是可以仅执行一次就结束排程的指令; 可能首先需要安装at并配置atd服务

启动 atd 服务
-------------

```
[root@www ~]# /etc/init.d/atd restart
正在停止 atd: [ 确定 ]
正在启动 atd: [ 确定 ]

# 再配置一下启动时就启动这个服务，免得每次重新启动都得再来一次！
[root@www ~]# chkconfig atd on
```

at命令
---------

> 我們使用 at 這個指令來產生所要運作的工作，並將這個工作以文字檔
的方式寫入 `/var/spool/at/` 目錄內，該工作便能等待 atd 這個服務的取用與執行了

```
[root@www ~]# at [-mldv] TIME
[root@www ~]# at -c 工作号码
```
### 选项与参数：

* -m ：当 at 的工作完成后，即使没有输出信息，亦以 email 通知使用者该工作已完成。
* -l ：相当於 atq，列出目前系统上面的所有该使用者的 at 排程；
* -d ：相当於 atrm ，可以取消一个在 at 排程中的工作；
* -v ：可以使用较明显的时间格式列出 at 排程中的工作列表；
* -c ：可以列出后面接的该项工作的实际命令内容。

### TIME

时间格式，定义『什么时候要进行 at 这项工作』，格式有：

* HH:MM    例如: 04:00
    * 在今日的 HH:MM 时刻进行，若该时刻已超过，则明天的 HH:MM 进行此工作。

* HH:MM YYYY-MM-DD   例如: 04:00 2009-03-17
    * 强制规定在某年某月的某一天的特殊时刻进行该工作！

* HH:MM[am|pm] [Month] [Date]  例如: 04pm March 17
    * 也是一样，强制在某年某月某日的某时刻进行！

* HH:MM[am|pm] + number [minutes|hours|days|weeks]
    * 例如:now + 5 minutes    例如: 04pm + 3 days
    * 就是说，在某个时间点『再加几个时间后』才进行。

### 范例

1. 再過五分鐘後，將 /root/.bashrc 寄給 root 自己

    ```
    [root@www ~]# at now + 5 minutes  <==記得單位要加 s 喔！
    at> /bin/mail root -s "testing at job" < /root/.bashrc
    at> <EOT>   <==這裡輸入 [ctrl] + d 就會出現 <EOF> 的字樣！代表結束！
    job 4 at 2009-03-14 15:38
    # 上面這行資訊在說明，第 4 個 at 工作將在 2009/03/14 的 15:38 進行！
    # 而執行 at 會進入所謂的 at shell 環境，讓你下達多重指令等待運作！
    ```

2. 將上述的第 4 項工作內容列出來查閱

    ```
    [root@www ~]# at -c 4
    #!/bin/sh 
    # atrun uid=0 gid=0
    # mail     root 0
    umask 22
    ....(中間省略許多的環境變數項目)....
    cd /root || {           <==可以看出，會到下達 at 時的工作目錄去執行指令
             echo 'Execution directory inaccessible' >&2
             exit 1
    }

    /bin/mail root -s "testing at job" < /root/.bashrc
    # 你可以看到指令執行的目錄 (/root)，還有多個環境變數與實際的指令內容啦！
    ```

3. 由於機房預計於 2009/03/18 停電，我想要在 2009/03/17 23:00 關機？

    ```
    [root@www ~]# at 23:00 2009-03-17
    at> /bin/sync
    at> /bin/sync
    at> /sbin/shutdown -h now
    at> <EOT>
    job 5 at 2009-03-17 23:00
    # at 還可以在一個工作內輸入多個指令呢！不錯吧！
    ```

### 输出

at 的运行与终端机环境无关，而所有 standard output/standard error output 都会传送到运行者的 mailbox 去啦！所以在终端机当然看不到任何资讯。

可以使用『 echo "Hello" > /dev/tty1 』来输出到指定的终端窗口中。

### 后台运行

at 有另外一个很棒的优点，那就是『后台运行』

### 权限安全

`/etc/at.allow` 和 `/etc/at.deny` 用于控制允许和禁止使用at命令的用户,allow优先级高;如果不存在这两个文件,则只有root可以使用.

一个帐号写一行。

batch命令
-------

> batch命令内部是通过at命令运行的。所以用法和at完全一样。唯一区别在于他会在 CPU 工作负载小於 0.8 的时候，才执行计划排程
