命名规范
===========

* 类名：首字母大写驼峰法
* 类文件：类名.class.php
* 成员属性和方法：首字母小写驼峰法(私有属性_前导)
* 函数、数据表和表字段：下划线分隔法。例如：get_domain()，think_user,user_name
* 常量、配置项、语言文件：全大写，下划线分隔

###类定义规则

|类库    |规则           |父类    |示例                                                    |
|--------|---------------|--------|--------------------------------------------------------|
|控制器类|模块名+Action  |Action  |UserAction、InfoAction                                  |
|模型类  |模型名+Model   |Model   |UserModel、InfoModel                                    |
|行为类  |行为名+Behavior|Behavior|CheckRouteBehavior                                      |
|Widget类|Widget名+Widget|Widget  |BlogInfoWidget                                          |
|驱动类  |引擎名+驱动名  |-       |DbMysql 表示 mysql 数据库驱动 CacheFile 表示文件缓存驱劢|



主入口文件
==========

入口文件需定义一些项目常量，并引入框架主文件

	define('THINK_PATH','../ThinkPHP/'); //目录以/结尾
	define('APP_PATH','./home/');//目录以/结尾
	define('APP_NAME', 'home');
	define('APP_DEBUG', true); //为true 开启调试模式
	require THINK_PATH."ThinkPHP.php";

其他可定义常量：RUNTIME_PATH 编译文件路径；RUNTIME_FILE定义编译文件路径，MODE_NAME应用类型，例如:cli

目录结构
===========

TP会根据APP_PATH自动创建项目目录及所需的子目录和文件，所以要确认Web服务器有相应的写权限

项目路径下，会生成以下子目录:

* **Common**  放置公共函数文件,common.php可被自动载入，其他函数文件需手动载入
* **Conf**  放置config.php,tags.php等配置文件
* **Lang**  放置语言包文件，每个语言包一个子目录
* **Lib**  放置项目需要的类文件，有Action和Model两个子目录分别防控制器控制类和模型类文件，其他类文件也要放在该目录的子目录下
* **Tpl**  放置模板
* **Runtime**  编译和缓存文件

其他目录可根据需要自行决定部署方式

项目配置
=======

> 配置是在全局生效并可在运行时改写的设置

> 配置文件格式：return 一个key/value配置数组。TP会统一将配置项参数转为小写

默认配置
----------

> 系统默认配置：`ThinkPHP/Conf` 下放置了系统的各类**默认配置**文件。

* **convention.php**  惯例配置，在项目中使用**config.php**进行覆盖配置
* **debug.php**  调试选项
* **tags.php**  行为选项
* **mode.php**  模式选项
* **alias.php** 别名配置

项目配置
----------

> 如果需要覆盖默认配置，只需在 `DocRoot/App/Conf/` 下或 `DocRoot/Conf/App/`下创建同名文件进行配置，运行是会自动载入，并可被编译到编译文件中

C函数
-----

> 用来读取配置值或者在运行时更改配置

<p class="api">mixed C(string|array $name=null[,string $value=null])</p>

* 如果只提供$name参数，且为字符串，则返回该项配置的的value
* 如果只提供$name参数，且为数组，则将该项配置更新为新值
* 如果无参数，返回所有配置项的数组
* $name 可以是`.`号隔开的字符串，表示二位数组配置项，例如：C('USER_CONFIG.USER_TYPE','1');

扩展配置
-----------

> 扩展配置与项目配置的不同在于，该类配置不会被编译进缓存，所以更改配置后，无需重新生成新缓存。

扩展配置可以自由定义配置php文件名，例如：user.php;db.php

然后在项目配置中，使用 `LOAD_EXT_CONFIG'=>'user,db` 进行配置，TP在项目运行时自动将user.php和db.php中的数组合并到配置文件中

如果使用：`LOAD_EXT_CONFIG'=>array('EXT_USER'=>'user,'EXT_DB'=>'db')`,则TP在运行时会将EXT_USER和EXT_DB作为两个项目配置，值就是文件返回的数组。最后需要这样获取配置：C('EXT_USER.NAME')

#####注意：{y:**LOAD_EXT_CONFIG**项必须写在 `APP/Conf/config.php` 文件中,扩展配置文件必须放在`APP/Conf/`目录下}

目录常量：
==========

> 路径设置 可在入口文件中重新定义 所有路径常量都必须以 / 结尾

> THINK_PATH，APP_PATH常量必须定义，其他常量可根据这两个常量自动定义

	//常量中源码的Common/runtime.php文件中定义
	defined('CORE_PATH')    or define('CORE_PATH',      THINK_PATH.'Lib/'); // 系统核心类库目录
	defined('EXTEND_PATH')  or define('EXTEND_PATH',    THINK_PATH.'Extend/'); // 系统扩展目录
	defined('RUNTIME_PATH') or define('RUNTIME_PATH',       APP_PATH.'Runtime/'); // 项目日志目录
	defined('MODE_PATH')    or define('MODE_PATH',      EXTEND_PATH.'Mode/'); // 模式扩展目录
	defined('ENGINE_PATH')  or define('ENGINE_PATH',    EXTEND_PATH.'Engine/'); // 引擎扩展目录
	defined('VENDOR_PATH')  or define('VENDOR_PATH',    EXTEND_PATH.'Vendor/'); // 第三方类库目录
	defined('LIBRARY_PATH') or define('LIBRARY_PATH',   EXTEND_PATH.'Library/'); // 扩展类库目录
	defined('COMMON_PATH')  or define('COMMON_PATH',    APP_PATH.'Common/'); // 项目公共目录
	defined('LIB_PATH')     or define('LIB_PATH',       APP_PATH.'Lib/'); // 项目类库目录
	defined('CONF_PATH')    or define('CONF_PATH',      APP_PATH.'Conf/'); // 项目配置目录
	defined('LANG_PATH')    or define('LANG_PATH',      APP_PATH.'Lang/'); // 项目语言包目录
	defined('TMPL_PATH')    or define('TMPL_PATH',      APP_PATH.'Tpl/'); // 项目模板目录
	defined('HTML_PATH')    or define('HTML_PATH',      APP_PATH.'Html/'); // 项目静态目录
	defined('LOG_PATH')     or define('LOG_PATH',       RUNTIME_PATH.'Logs/'); // 项目日志目录
	defined('TEMP_PATH')    or define('TEMP_PATH',      RUNTIME_PATH.'Temp/'); // 项目缓存目录
	defined('DATA_PATH')    or define('DATA_PATH',      RUNTIME_PATH.'Data/'); // 项目数据目录
	defined('CACHE_PATH')   or define('CACHE_PATH',     RUNTIME_PATH.'Cache/'); // 项目模板缓存目录