Version 6新功能
=============

新功能
---------

* Refactor 增加了 {g:Change  Signature...} 功能

* Code 增加了 {g:Rearranger},可以重新对成员排序.排序方式,在project settings->code styles->php->arrangement设置

* Romote Host Toolbar 增加了与 Project Toolbar 之间文件的拖拽,复制粘贴功能

* 增加了对于PHP的 {g:Optimize Imports} 功能,可以优化 php5.4的 use 语句导入的文件

	1. 可以移除没有用到的导入语句
	
	2. 对导入语句进行排序
	
	3. Splits multiple use statement imports into single use statement imports.
	
* {g:Pull members up/Push members down}  可以把成员移动到基类中,也可以把成员从基类移动到子类

* Tools -> {g:Test RESTful webservice} 用于调试 RESTful 网络服务

* {g:File Watchers} 插件,用于转换Sass, LESS, SCSS, CoffeeScript, TypeScript

* 当 new 一个其他命名空间的class时,会自动在前面加入导入该命名空间类的use语句


增强
--------

* 自动完成,实时问题检测

* 新的 database schema editor(包括查询排序,ddl详情,完整的col编辑功能) 和 change tracking tools

* 完全自定义的模板功能,包括phpdoc模板

* HTML structure 支持HTML5

* Refactor -> Move 增加了对Class namespace的支持,可以将类移动到另一个命名空间

* 当创建新Class时,可以侦测到当前的命名空间,自动填写命名空间字段