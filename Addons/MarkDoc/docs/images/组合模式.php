<?php
abstract class Unit{
	abstract function common_method();
	public function is_composite(){  //可见性必须为public 否则客户端无法访问此方法
		return null; //返回null表示是个基本单元
	}
}

/* 最小单元*/
class Archer extends Unit{
	function common_method(){
		return 4; //射手的战斗力
	}
}
class Tank extends Unit{
	function common_method(){
		return 400; //坦克的战斗力
	}
}

/*组合单元类*/
abstract class CompositeUnit extends Unit{
	protected $units=array(); // 切勿设置为priavte,否则无法从容器对象对象中访问到属性了
	function add_unit(Unit $second){
		if ( in_array($second,$this->units,true) ){
			return;
		}
		$this->units[]=$second;
	}
	function remove_unit(Unit $second){
		// var_dump( $this);
		$this->units = array_udiff($this->units,array($second),
			function($a,$b){  //回调函数取差集
				if ($a===$b){
					return 0;
				}elseif($a>$b){
					return 1;
				}else{
					return -1;
				}
			}
		);
	}
	public function is_composite(){  //可见性必须为public 否则客户端无法访问此方法
		return $this; //返回自身，表示是一个组合单元
	}
}
class Army extends CompositeUnit{
	function common_method(){
		$set = 0;
		foreach ( $this->units as $unit){
			$set += $unit->common_method();
		}
		return $set;
	}
}

/*
 *用户操作客户端
 */ 
class client{
	/**
	 * dounits()
	 * 组合对象或移除对象
	 * @param  $first 		欲与第二个对象组合，或欲从此对象中移除包含的第二个对象
	 * @param  $second 		欲加入或移除的对象
	 * @param  $contrl 		值为1表示要组合进第一个对象，值为0表示欲移除此对象__halt_compiler
	 * @return  结果对象
	 */
	static function dounits (Unit $first,Unit $second,$contrl=1){
		if(!is_null($first->is_composite())){
			$composite = $first;
		}else{
			$composite=new Army();//如果两个对象都是最小单元，生成一个容器组合对象
			$composite->add_unit($first);
		}
		if ($contrl>0){
			$composite->add_unit($second);
		}else{
			$composite->remove_unit($second);
		}
		return $composite;
	}
}
$archer = new Archer();
$tank = new Tank();
$army = client::dounits($archer,$tank);//通过客户端组合两个对象到为一个集合
echo $army->common_method(); //输出集合的战斗力
?>