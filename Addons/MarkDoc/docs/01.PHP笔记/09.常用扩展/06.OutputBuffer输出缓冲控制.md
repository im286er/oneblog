PHP.ini设置
---------------

```ini
;输出缓冲区大小php.ini设置
output_buffering = 4096 //bytes

;是否在输出时自动进行压缩
zlib.output_compression
```

!!!!!输出缓冲允许嵌套(就像html标签嵌套一样)，但注意保证每个ob_start()都对应一个ob_end_flush()或者ob_end_clean()。

<p class="api">bool ob_start([callable $output_callback[,int $chunk_size=0[,bool $erase=true]]])</p>

打开PHP的缓冲区，此后没有输出能从脚本送出（除http标头外），输出的内容被存储在PHP的缓冲区中。

一般情况无需使用参数

$output_callback
:   回调函数用于在输出时过滤处理缓冲区中的内容，他的第一个参数接收到的是字符串，返回的是处理过后的字符串。他在执行ob_fluch或ob_clean之类的函数之前或者缓冲区在脚本结束时输出到浏览器时被自动调用。{y:如果$output_callback设为null,则忽略该项}。
:   如果它返回false,那么缓冲区的原始数据被输出。另外，你不能在回调函数中调用ob函数

$chunk_size
:   设定一个长度，当缓冲区内容到达这个长度时自动输出。默认为0，表示不限制。

$erase
:   缓冲输出后是否清除缓冲中已输出的内容

!!!!!Some web servers (e.g. Apache) change the working directory of a script when calling the callback function. You can change it back by e.g. chdir(dirname($_SERVER['SCRIPT_FILENAME'])) in the callback function.

<p class="api">string ob_gzhandler(string $buffer,int $mode)</p>

它可以用作ob_start的回调函数，对缓冲区内容执行gzip压缩

!!!!!你不能同时使用 ob_gzhandler() 和 zlib.output_compression，所以在调用之前应该检测zlib.output_compression配置


<p class="api">string ob_iconv_handler(string $contents,int $status)</p>

作为ob_start的回调函数使用，对PHP缓冲区的内容执行编码转换

```php
iconv_set_encoding("internal_encoding", "UTF-8");
iconv_set_encoding("output_encoding", "ISO-8859-1");
ob_start("ob_iconv_handler"); // start output buffering
```

<p class="api">string mb_output_handler(string $contents,int $status)</p>

作为ob_start的回调函数使用，将输出缓冲中的字符从内部字符编码转换为 HTTP 输出的字符编码。

```php
mb_http_output("UTF-8");
ob_start("mb_output_handler");
```

<p class="api">int ob_get_length(void)</p>

此函数将返回输出缓中冲区内容的长度。

<p class="api">string ob_get_contents(void)</p>

只是得到输出缓冲区的内容，但不清除它。

<p class="api">string ob_get_clean(void)</p>

得到当前缓冲区的内容并删除当前输出缓冲区。

<p class="api">void ob_flush(void)</p>

这个函数将输出所有缓冲区的内容（如果里边有内容的话），并清空缓冲区，但缓冲区会保留，可以继续使用。

<p class="api">void ob_clean(void)</p>

此函数用来丢弃所有输出缓冲区中的内容。并保留缓冲区。

<p class="api">ob_get_level(void)</p>

返回缓冲区的嵌套层级，顶层返回1

<p class="api">array ob_get_status([bool $full_status=FALSE])</p>

返回当前缓冲区的状态信息，默认返回当前缓冲区的状态：

```php
Array
(
    [level] => 2 //嵌套层级
    [type] => 0  //0，内部回调；或者1，用户自定义回调
    [status] => 0 //PHP_OUTPUT_HANDLER_START (0), PHP_OUTPUT_HANDLER_CONT (1) or PHP_OUTPUT_HANDLER_END (2)
    [name] => URL-Rewriter //回调函数名，如果未指定，值为："default output handler"
    [del] => 1//ob_start的$erase参数值
)
```

如果$full_status设为true,则用二维索引数组返回从顶层到当前层的所有缓冲区的状态，顶层的索引是0：

```php
Array
(
    [0] => Array
        (
            [chunk_size] => 0
            [size] => 40960
            [block_size] => 10240
            [type] => 1
            [status] => 0
            [name] => default output handler
            [del] => 1
        )

    [1] => Array
        (
            [chunk_size] => 0
            [size] => 40960
            [block_size] => 10240
            [type] => 0
            [buffer_size] => 0
            [status] => 0
            [name] => URL-Rewriter
            [del] => 1
        )

)
```


<p class="api">bool ob_end_flush(void)</p>

这个函数将输出最顶层缓冲区的内容（如果里边有内容的话），并关闭缓冲区

<p class="api">string ob_get_flush(void)</p>

与ob_end_flush 不同在于，输出之后她还返回输出的内容。

<p class="api">bool ob_end_clean(void)</p>

此函数丢弃最顶层输出缓冲区的内容并关闭这个缓冲区。

<p class="api">bool fflush(resource $handle)</p>

将缓冲区的内容写入到以打开的文件资源中

<p class="api">bool output_add_rewrite_var(string $name,string $value)</p>

在输出缓冲内容时，对url_rewriter.tags 设定的标签中的url添加queryString

!!!!!对于绝对url不会添加

```php
output_add_rewrite_var('var', 'value');

// some links
echo '<a href="file.php">link</a>
<a href="http://example.com">link2</a>';
/*输出：
<a href="file.php?var=value">link</a>
<a href="http://example.com">link2</a>
*/
```

<p class="api">bool output_reset_rewrite_vars(void)</p>

取消之前output_add_rewrite_var的设定或者session.use_trans_sid的设定


<p class="api">void flush(void)</p>

对于一些执行时间比较长的脚本，你可以在输出内容产生后，立即通知**webserver**把输出的内容发送到浏览器，而不必等到脚本执行结束。

!!!!!如果使用了ob_start,那么flush函数应该放**在ob_输出函数之后调用**。

深入理解ob_flush和flush的区别
------------------------------

[原文链接](http://www.laruence.com/2010/04/15/1414.html)

ob_flush/flush在手册中的描述, 都是刷新输出缓冲区, 并且还需要配套使用, 所以会导致很多人迷惑…

其实, 他们俩的操作对象不同, 有些情况下, flush根本不做什么事情..

ob_*系列函数, 是操作PHP本身的输出缓冲区.所以, ob_flush是刷新PHP自身的缓冲区.

而flush, {y:严格来讲, 这个只有在PHP做为apache的Module(handler或者filter)安装的时候, 才有实际作用}. 它是刷新WebServer(可以认为特指apache)的缓冲区.

在apache module的sapi下, flush会通过调用sapi_module的flush成员函数指针, 间接的调用apache的api: ap_rflush刷新apache的输出缓冲区, 当然手册中也说了, 有一些apache的其他模块, 可能会改变这个动作的结果..

有些Apache的模块，比如mod_gzip，可能自己进行输出缓存，
这将导致flush()函数产生的结果不会立即被发送到客户端浏览器。 
 
甚至浏览器也会在显示之前，缓存接收到的内容。例如 Netscape
浏览器会在接受到换行或 html 标记的开头之前缓存内容，并且在
接受到 </table> 标记之前，不会显示出整个表格。 
 
一些版本的 Microsoft Internet Explorer 只有当接受到的256个
字节以后才开始显示该页面，所以必须发送一些额外的空格来让这
些浏览器显示页面内容。
所以, 正确使用俩者的顺序是. 先ob_flush, 然后flush,

当然, 在其他sapi下, 不调用flush也可以, 只不过为了保证你代码的可移植性, 建议配套使用.