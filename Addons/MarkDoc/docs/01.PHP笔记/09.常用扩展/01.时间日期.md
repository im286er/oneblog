时区设置
------------

1.php.ini

date.timezone = PRC  ;设置为北京时间

date.timezone = UTC  ;通用标准时间

<p class="api">string date_default_timezone_get(void)</p>

本函数返回默认时区，使用如下“假定”的顺序：

用 date_default_timezone_set() 函数设定的时区（如果设定了的话）

TZ 环境变量（如果非空）

date.timezone 配置选项（如果设定了的话）

自己推测（如果操作系统支持）

如果以上选择都不成功，则返回 UTC

<p class="api">bool date_default_timezone_set(string $timezone_identifier)</p>

中国的时区标识是：Asia/Shanghai  Asia/Chongqing

香港：Asia/Hong_Kong

台湾：Asia/Taipei

其他时区可查阅手册的函数参考-时间和日期扩展-支持的时区

创建时间戳
--------------

<p class="api">int time(void)</p>

返回当前本地时间戳（格林威治时间 1970 年 1 月 1 日 00:00:00开始的秒数）

<p class="api">int mktime([int $hour=date("H")[,int $minute=date("i")[,int $second=date("s")[,int $month=date("n")[,int $day=date("j")[,int $year=date("Y")[,int $is_dst=-1]]]]]]])</p>

返回指定的时间的 Unix 本地时间戳。

mktime() 在做日期计算和验证方面很有用，它会自动计算超出范围的输入的正确值。例如下面例子中每一行都会产生字符串 "Jan-01-1998"。
```php
echo date("M-d-Y", mktime(0, 0, 0, 12, 32, 1997));
echo date("M-d-Y", mktime(0, 0, 0, 13, 1, 1997));
echo date("M-d-Y", mktime(0, 0, 0, 1, 1, 1998));
echo date("M-d-Y", mktime(0, 0, 0, 1, 1, 98));
```

每个月的第0天，是上个月的最后一天，-1天是上个月的倒数第二天，以此类推：

```php
$lastday = mktime(0, 0, 0, 3, 0, 2000);
echo strftime("Last day in Feb 2000 is: %d", $lastday);
$lastday = mktime(0, 0, 0, 4, -31, 2000);
echo strftime("Last day in Feb 2000 is: %d", $lastday);
```

<p class="api">mixed microtime([bool $as_float=false])</p>

当前 Unix 时间戳以及微秒数。本函数仅在支持 gettimeofday() 系统调用的操作系统下可用。

如果调用时不带可选参数，本函数以 "msec sec" 的格式返回一个字符串，其中 sec 是自 Unix 纪元（0:00:00 January 1, 1970 GMT）起到现在的秒数，msec 是微秒部分。字符串的两部分都是以秒为单位返回的。

microtime(true) 将返回一个“sec.msec”格式的浮点数。


<p class="api">int strtotime(string $time[,int $now])</p>


本函数预期接受一个包含美国英语日期格式的字符串并尝试将其解析为 Unix 时间戳（自 January 1 1970 00:00:00 GMT 起的秒数），其值相对于 now 参数给出的时间，如果没有提供此参数则用系统当前时间。

```php
echo strtotime("now"), "\n";
echo strtotime("10 September 2000"), "\n";
echo strtotime("+1 day"), "\n";
echo strtotime("+1 week"), "\n";
echo strtotime("+1 week 2 days 4 hours 2 seconds"), "\n";
echo strtotime("next Thursday"), "\n";
echo strtotime("last Monday"), "\n";
```

返回时间
-------------

> 可以返回格式化的时间字符串，也可以返回时间信息的数组

<p class="api">string date ( string $format [, int $timestamp ] )</p>

把时间戳转化为格式化的时间，如果未提供时间戳，则为当前的时间戳

在格式字串中的字符前加上反斜线来转义可以避免它被按照上表解释。如果加上反斜线后的字符本身就是一个特殊序列，那还要转义反斜线（例如要输出n，则需要这样写:\\n）。

|format字符        |说明                                                                                                |返回值例子                                                                                                      |
|------------------|----------------------------------------------------------------------------------------------------|-----------                                                                                                     |
|日                |---                                                                                                 |---                                                                                                             |
|d                 |月份中的第几天，有前导零的 2 位数字                                                                 |01 到 31                                                                                                        |
|D                 |星期中的第几天，文本表示，3 个字母                                                                  |Mon 到 Sun                                                                                                      |
|j                 |月份中的第几天，没有前导零                                                                          |1 到 31                                                                                                         |
|l                 |（“L”的小写字母） 星期几，完整的文本格式                                                            |Sunday 到 Saturday                                                                                              |
|N                 |ISO-8601 格式数字表示的星期中的第几天（PHP 5.1.0 新加）                                             |1（表示星期一）到 7（表示星期天）                                                                               |
|S                 |每月天数后面的英文后缀，2 个字符                                                                    |st，nd，rd 或者 th。可以和 j 一起用                                                                             |
|w                 |星期中的第几天，数字表示                                                                            |0（表示星期天）到 6（表示星期六）                                                                               |
|z                 |年份中的第几天                                                                                      |0 到 366                                                                                                        |
|星期              |---                                                                                                 |---                                                                                                             |
|W                 |ISO-8601 格式年份中的第几周，每周从星期一开始（PHP 4.1.0 新加的）                                   |例如：42（当年的第 42 周）                                                                                      |
|月                |---                                                                                                 |---                                                                                                             |
|F                 |月份，完整的文本格式，例如 January 或者 March                                                       |January 到 December                                                                                             |
|m                 |数字表示的月份，有前导零                                                                            |01 到 12                                                                                                        |
|M                 |三个字母缩写表示的月份                                                                              |Jan 到 Dec                                                                                                      |
|n                 |数字表示的月份，没有前导零                                                                          |1 到 12                                                                                                         |
|t                 |给定月份所应有的天数                                                                                |28 到 31                                                                                                        |
|年                |---                                                                                                 |---                                                                                                             |
|L                 |是否为闰年                                                                                          |如果是闰年为 1，否则为 0                                                                                        |
|o                 |ISO-8601 格式年份数字。这和 Y 的值相同，只除了如果 ISO 的星期数（W）属于前一年或下一年，则用那一年  |Examples: 1999 or 2003                                                                                          |
|Y                 |4 位数字完整表示的年份                                                                              |例如：1999 或 2003                                                                                              |
|y                 |2 位数字表示的年份                                                                                  |例如：99 或 03                                                                                                  |
|时间              |---                                                                                                 |---                                                                                                             |
|a                 |小写的上午和下午值                                                                                  |am 或 pm                                                                                                        |
|A                 |大写的上午和下午值                                                                                  |AM 或 PM                                                                                                        |
|B                 |Swatch Internet                                                                                     |标准时 000 到 999                                                                                               |
|g                 |小时，12 小时格式，没有前导零                                                                       |1 到 12                                                                                                         |
|G                 |小时，24 小时格式，没有前导零                                                                       |0 到 23                                                                                                         |
|h                 |小时，12 小时格式，有前导零                                                                         |01 到 12                                                                                                        |
|H                 |小时，24 小时格式，有前导零                                                                         |00 到 23                                                                                                        |
|i                 |有前导零的分钟数                                                                                    |00 到 59                                                                                                        |
|s                 |秒数，有前导零                                                                                      |00 到 59                                                                                                        |
|时区              |---                                                                                                 |---                                                                                                             |
|e                 |时区标识（PHP 5.1.0 新加）                                                                          |例如：UTC，GMT，Atlantic/Azores                                                                                 |
|I                 |是否为夏令时                                                                                        |如果是夏令时为 1，否则为 0                                                                                      |
|O                 |与格林威治时间相差的小时数                                                                          |例如：+0200                                                                                                     |
|P                 |与格林威治时间（GMT）的差别，小时和分钟之间有冒号分隔（PHP 5.1.3 新加）                             |例如：+02:00                                                                                                    |
|T                 |本机所在的时区                                                                                      |例如：EST，MDT（【译者注】在 Windows 下为完整文本格式，例如“Eastern Standard Time”，中文版会显示“中国标准时间”）|
|Z                 |时差偏移量的秒数。UTC 西边的时区偏移量总是负的，UTC 东边的时区偏移量总是正的。                      |-43200 到 43200                                                                                                 |
|完整的日期／时间  |---                                                                                                 |---                                                                                                             |
|c                 |ISO 8601 格式的日期（PHP 5 新加）                                                                   |2004-02-12T15:19:21+00:00                                                                                       |
|r                 |RFC 822 格式的日期                                                                                  |例如：Thu, 21 Dec 2000 16:01:07 +0200                                                                           |
|U                 |从 Unix 纪元（January 1 1970 00:00:00 GMT）开始至今的秒数 参见 time()                               |---                                                                                                             |  






格式字串中不能被识别的字符将原样显示。Z 格式在使用 gmdate() 时总是返回 0。

<p class="api">array getdate([int $timestamp])</p>

根据时间戳返回时间信息数组。如果没有给出时间戳则认为是当前本地时间。数组中的单元如下：

返回的关联数组中的键名单元

|键名       |说明                                                                        |返回值例子                                      |
|-----------|----------------------------------------------------------------------------|------------------------------------------------|
|"seconds"  |秒的数字表示                                                                |0 到 59                                         |
|"minutes"  |分钟的数字表示                                                              |0 到 59                                         |
|"hours"    |小时的数字表示                                                              |0 到 23                                         |
|"mday"     |月份中第几天的数字表示                                                      |1 到 31                                         |
|"wday"     |星期中第几天的数字表示                                                      |0（表示星期天）到 6（表示星期六）               |
|"mon"      |月份的数字表示                                                              |1 到 12                                         |
|"year"     |4 位数字表示的完整年份                                                      |例如：1999 或 2003                              |
|"yday"     |一年中第几天的数字表示                                                      |0 到 365                                        |
|"weekday"  |星期几的完整文本表示                                                        |Sunday 到 Saturday                              |
|"month"    |月份的完整文本表示                                                          |January 到 December                             |
|0          |自从 Unix 纪元开始至今的秒数，和 time() 的返回值以及用于 date() 的值类似。  |系统相关，典型值为从 -2147483648 到 2147483647。|




<p class="api">string strftime(string $format[,int $timestamp])</p>

与date()函数相似，$format的表示方式不同，并且根据区域设置使用设定区域的语言（而不一定是英文）和时间格式化本地时间／日期


|格式          |描述                                                                                                                                                                         |返回值示例                                                      |
|--------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------                                                   |
|日            |---                                                                                                                                                                          |---                                                             |
|%a            |当前区域星期几的简写                                                                                                                                                         |Sun 到 Sat                                                      |
|%A            |当前区域星期几的全称                                                                                                                                                         |Sunday 到 Saturday                                              |
|%d            |月份中的第几天，十进制数字（范围从 01 到 31）                                                                                                                                |01 到 31                                                        |
|%e            |月份中的第几天，十进制数字，一位的数字前会加上一个空格（范围从 ' 1' 到 '31'） 在 Windows 上尚未按描述实现。更多信息见下方。                                                  |1 到 31                                                         |
|%j            |年份中的第几天，带前导零的三位十进制数（范围从 001 到 366）                                                                                                                  |001 到 366                                                      |
|%u            |符合 ISO-8601 星期几的十进制数表达 [1,7]，1 表示星期一                                                                                                                       |1 (星期一) 到 7 (星期日)                                        |
|%w            |星期中的第几天，星期天为 0                                                                                                                                                   |0 (星期天) 到 6 (星期六)                                        |
|周            |---                                                                                                                                                                          |---                                                             |
|%U            |本年的第几周，从第一周的第一个星期天作为第一天开始                                                                                                                           |13 (for the 13th full week of the year)                         |
|%V            |%V - 本年第几周的 ISO-8601:1988 格式，范围从 01 到 53，第 1 周是本年第一个至少还有 4 天的星期，星期一作为每周的第一天。（用 %G 或者 %g 作为指定时间戳相应周数的年份组成。）  |01 到 53 (where 53 accounts for an overlapping week)            |
|%W            |本年的第几周数，从第一周的第一个星期一作为第一天开始                                                                                                                         |46 (for the 46th week of the year beginning with a Monday)      |
|月            |---                                                                                                                                                                          |---                                                             |
|%b            |当前区域月份的简写                                                                                                                                                           |Jan 到 Dec                                                      |
|%B            |当前区域月份的全称                                                                                                                                                           |January 到 December                                             |
|%h            |当前区域月份的简写                                                                                                                                                           |（%b 的别名） Jan 到 Dec                                        |
|%m            |两位数的月份                                                                                                                                                                 |01 (是一月份) 到 12 (是十二月份)                                |
|年            |--- ---                                                                                                                                                                      |---                                                             |
|%C            |两位数显示世纪（年份除以 100，截成整数）                                                                                                                                     |19 是 20 世纪                                                   |
|%g            |2 位数的年份，符合 ISO-8601:1988 星期数（参见 %V）。和 %V 的格式和值一样，只除了如果 ISO 星期数属于前一年或者后一年，则使用那一年。                                          |比如：2009年1月6日那一周是 09。                                 |
|%G            |%g 的完整四位数版本                                                                                                                                                          |比如：2009年1月3日那一周是 2008.                                |
|%y            |两位数显示年份                                                                                                                                                               |比如： 09 是 2009，79 是 1979                                   |
|%Y            |四位数显示年份                                                                                                                                                               |比如： 2038                                                     |
|时间          |---                                                                                                                                                                          |---                                                             |
|%H            |以 24 小时格式显示两位小时数                                                                                                                                                 |00 到 23                                                        |
|%I            |以 12 小时格式显示两位小时数                                                                                                                                                 |01 到 12                                                        |
|%l            |（'L' 的小写） 以 12 小时格式显示小时数，单个数字前含空格                                                                                                                    |1 到 12                                                         |
|%M            |两位的分钟数                                                                                                                                                                 |00 到 59                                                        |
|%p            |指定时间的大写 “AM” 或 “PM”                                                                                                                                                  |比如： 00:31 是 AM ，22:23 是PM                                 |
|%P            |指定时间的小写 “am” 或 “pm”                                                                                                                                                  |比如：00:31 是 am ，22:23 是pm                                  |
|%r            |和 "%I:%M:%S %p" 一样                                                                                                                                                        |比如： 21:34:17 是 09:34:17 PM                                  |
|%R            |和 "%H:%M" 一样                                                                                                                                                              |比如： 12:35 AM 是 00:35，4:44 PM 是 16:44                      |
|%S            |两位数字表示秒                                                                                                                                                               |00 到 59                                                        |
|%T            |和 "%H:%M:%S" 一样                                                                                                                                                           |比如： 09:34:17 PM 是 21:34:17                                  |
|%X            |当前区域首选的时间表示法，不包括日期                                                                                                                                         |例如： 03:59:16 或 15:59:16                                     |
|%z            |从 UTC 的时区偏移 或 简写（由操作系统决定）                                                                                                                                  |比如： 东部时间是 -0500 或 EST                                  |
|%Z            |%z 没有给出的 UTC 的时区偏移 或 简写（由操作系统决定）                                                                                                                       |比如： -0500 或 EST 是东部时间                                  |
|时间和日期戳  |---                                                                                                                                                                          |---                                                             |
|%c            |当前区域首选的日期时间表达                                                                                                                                                   |比如： 2009 年 2 月 5 日上午 12:45:10 是 Tue Feb 5 00:45:10 2009|
|%D            |和 "%m/%d/%y" 一样                                                                                                                                                           |比如： 2009 年 2 月 5 日是 02/05/09                             |
|%F            |Same as "%Y-%m-%d" (commonly used in database datestamps)                                                                                                                    |比如：2009 年 2 月 5 日是 2009-02-05                            |
|%s            |Unix纪元的时间戳（和 time() 函数一样）                                                                                                                                       |比如： 1979 年 9 月 10 日上午 8 点 40 分 00 秒是 305815200      |
|%x            |当前区域首选的时间表示法，不包括时间                                                                                                                                         |比如： 2009 年 2 月 5 日是 02/05/09                             |
|其他          |---                                                                                                                                                                          |---                                                             |
|%n            |换行符("\n")                                                                                                                                                                 |---                                                             |
|%t            |Tab 字符("\t")                                                                                                                                                               |---                                                             |
|%%            |文字上的百分字符("%")                                                                                                                                                        |aaaa                                                            |




!!!!!尽管 ISO 9889:1999（当前的 C 标准）明确指出一周从星期一开始，但是 Sun Solaris 的一周似乎从星期天开始并作为 1。所以 %u 的结果也许不会和手册里描述得一样。

!!!!!Warning 仅针对 Windows：这个函数里 %e 修饰符修饰符还不能支持 Windows。 为了得到这个值可以用 %#d 修饰符来代替。下例说明了如何写一个跨平台支持的函数。

!!!!!Warning 仅针对 Mac OS X：这个函数里 %P 修饰符还不能支持 Mac OS X。


<p class="api">array localtime([int $timestamp=time()[,bool $is_associative=false]])</p>

与getdate()类似，只是默认以索引数组返回，以关联数组返回的时候键名是tm_sec这样的。

日期验证
------------

<p class="api">bool checkdate(int $month,int $day,int $year)</p>

检测一个日期是否是实际存在的日期

如果给出的日期有效则返回 TRUE，否则返回 FALSE。检查由参数构成的日期的合法性。日期在以下情况下被认为有效：

year 的值是从 1 到 32767  

month 的值是从 1 到 12  

Day 的值在给定的 month 所应该具有的天数范围之内，闰年已经考虑进去了。 

```php
var_dump(checkdate(12, 31, 2000));
var_dump(checkdate(2, 29, 2001));
/*
以上例程会输出：
bool(true)
bool(false)
*/
```

格林威治时间函数
----------------

> 某些协议(http协议)只支持GMT时间

<p class="api">int gmmktime([int $hour=gmdate("H")[,int $minute=gmdate("i")[,int $second=gmdate("s")[,int $month=gmdate("n")[,int $day=gmdate("j")[,int $year=gmdate("Y")[,int $is_dst=-1]]]]]]])</p>

返回本地时间对应的格林威治时间戳(GMT)

Note: gmmktime() 内部使用了 mktime()，因此只有转换成本地时间也合法的时间才能用在其中

<p class="api">string gmdate(string $format[,int $timestamp=time()])</p>

将本地时间戳转按格式转换为格林威治时间字符串

<p class="api">string gmstrftime(string $format[,int $timestamp=time()])</p>

设置本地语言
--------------

> 设置以后，时间日期的语言花表示将使用设定的语言。字符集为该语言在该操作系统上的字符集，例如在windows中，中文是以GBK字符集返回。而linux是以utf-8字符集返回，所以使用该函数时，必须考虑操作系统语言的字符集。

<p class="api">string setlocale(int $category,string $locale[,string $...])</p>

<p class="api">string setlocale(int $category,array $locale)</p>

$category

- `LC_ALL` for all of the below
- `LC_COLLATE` for string comparison, see strcoll()
- `LC_CTYPE` for character classification and conversion, for example strtoupper()
- `LC_MONETARY` for localeconv()
- `LC_NUMERIC` for decimal separator (See also localeconv())
- `LC_TIME` for date and time formatting with strftime()
- `LC_MESSAGES` for system responses (available if PHP was compiled with libintl)

$locale参数支持的设置字符串与平台有关：

windows平台详见：http://msdn.microsoft.com/en-us/library/39cwe7zf%28v=vs.90%29.aspx

