<p class="api">void usleep(int $micro_seconds)</p>

延迟运行一定微秒数。

<p class="api">int sleep(int $seconds)</p>

延迟运行一定秒数。

<p class="api">bool time_sleep_until(float $timestamp)</p>

延迟直到某个时间戳唤醒

<p class="api">void set_time_limit(int $seconds)</p>

设置脚本的超时时间，设为0表示不超时

<p class="api">string set_include_path(string $new_include_path)</p>

设置include搜索路径，可以在一个字符串中有多个路径。

例如：set_include_path('t1'.PATH_SEPARATOR.'t2');

<p class="api">array get_included_files(void)</p>

<p class="api">string get_include_path(void)</p>

<p class="api">void restore_include_path(void)</p>

将include路径恢复为ini中的设置

<p class="api">string php_ini_loaded_file(void)</p>

返回生效的php.ini文件的路径

<p class="api">int memory_get_usage([bool $real_usage=false])</p>

返回使用的内存大小

string ini_set ( string $varname , string $newvalue )

<p class="api">void ini_restore(string $varname)</p>

<p class="api">string ini_get(string $varname)</p>

<p class="api">array get_loaded_extensions([bool $zend_extensions=false]</p>

<p class="api">bool extension_loaded(string $name)</p>

<p class="api">string get_current_user(void)</p>

<p class="api">int gc_collect_cycles(void)</p>

强制立即执行垃圾回收

<p class="api">void register_shutdown_function(callable $callback[,mixed $parameter[,mixed $...]])</p>

注册在exit()方法调用时或脚本执行结束时的方法