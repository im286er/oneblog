装饰模式
==========

> 解决这样的问题：一个类的实例，可能需要多个不同的方法中的某几个方法依次进行处理。就像流水线装配产品一样，每个工位只做一件事，做完了传递给下一个工位。

实现方式1：我们可以把这些方法都定义在一个类中，让他们都返回$this以执行链式访问，只在最后一个方法返回最终结果。

实现方式2：装饰模式

![装饰模式](images/zhuangshimoshi.png)

```php
$sample1 = new Sample($data1);
$sample2 = new Sample($data2);

//处理一个实例
$result1= new Class5( new Class3( new MainProcess ) );
$result1=$result1->process($sample1);
//处理另一个实例
$result2= new Class4( new Class3( new MainProcess ) );
$result2=$result2->process($sample2);
```

#### 装饰器模式特点:

* 所有处理方法的类,都是同一个接口的实现.
* 必须有一个主处理方法类直接继承自该接口,用于处理流程的完结处理.
* 工位处理类都是一个构造类的子类。
* 实例化时,根据需要用到的类,按顺序嵌套实例化，最内层是主处理类，也就是最后一步要用到的对象。


#### 装饰模式与链式访问比较

它们都可以对数据进行分步处理，但是如果工序相同，链式访问每次处理不同的数据都需要完整写一次链式调用：

```
$result1= $obj->data($data1)->process1()->process2();
$result2= $obj->data($data2)->process1()->process2();
$result3= $obj->data($data3)->process1()->process2();
```

而装饰器模式则相对简单：

```
$obj= new Process1(new Process2());
$result1 = $obj->process($data1);
$result2 = $obj->process($data2);
$result3 = $obj->process($data3);
```