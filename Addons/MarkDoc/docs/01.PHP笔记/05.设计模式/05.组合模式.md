组合模式
=========

> 组合模式定义了一个隐性的单根树状体系，{g:组合模式中所有节点实例都源自同一接口，每个节点都是一个基本节点或若干个其他节点的组合}，组合模式的接口为所有节点定义了共同支持的功能，这些共同功能通常因组合而得到增强或减弱。

#### 组合模式的结构中，有两种类型单元：

1. 最小的单元，即不含有任何其他对象，不具有组合和移除其他单元的功能；
1. 组合单元，内部组合了其他单元，具有移除和组合其他单元的功能。

组合单元中包含的基层的对象发生变动时，组合单元必然也会受到影响。所以使用时，应确信你需要这样的情况发生，例如：一个士兵的战斗力变强，必然包含这个士兵的排战斗力增加，包含这个排的连战斗力增加。

#### 组合和移除最小单元前必须执行检测逻辑：

* 在使用A单元组合B单元时，先进行检测，如果A是组合单元，才可以组合B单元。如果A是最小单元，则可以new一个空的组合单元，把A和B依次组合入。
* 要从A单元移除内部的单元时，要检测A是不是组合单元，如果是才能执行移除功能。如果不是，是没有移除功能的。


#### 如下图：

抽象类Unit的子类都是最小单元，所以让抽象类中的isComposite()方法返回null。

抽象类CompositeUnit本身并没有抽象方法，他继承了Unit的抽象方法，该类只提供三个方法，分别是移除对象，添加对象，检测对象。

Unit的所有子类的实例，都可以被组合入CompositeUnit子类的对象中。所有的对象都拥有一个共同的方法common_method()返回自己的战斗力。

![组合模式](images/zuhemoshi.png)


小结：组合模式对于主要特性相似的简单对象非常便于操作，对象树可以方便地遍历。但如果规则变得复杂，代码会变得越来越难以维护。组合模式不能很好地在关系型数据库中保存数据，但非常适合用XML持久化。 

#### 实例：

```php
<?php
abstract class Unit{
	abstract function common_method();
	public function is_composite(){  //可见性必须为public 否则客户端无法访问此方法
		return null; //返回null表示是个基本单元
	}
}

/* 最小单元*/
class Archer extends Unit{
	function common_method(){
		return 4; //射手的战斗力
	}
}
class Tank extends Unit{
	function common_method(){
		return 400; //坦克的战斗力
	}
}

/*组合单元类*/
abstract class CompositeUnit extends Unit{
	protected $units=array(); // 切勿设置为priavte,否则无法从容器对象对象中访问到属性了
	function add_unit(Unit $second){
		if ( in_array($second,$this->units,true) ){
			return;
		}
		$this->units[]=$second;
	}
	function remove_unit(Unit $second){
		// var_dump( $this);
		$this->units = array_udiff($this->units,array($second),
			function($a,$b){  //回调函数取差集
				if ($a===$b){
					return 0;
				}elseif($a>$b){
					return 1;
				}else{
					return -1;
				}
			}
		);
	}
	public function is_composite(){  //可见性必须为public 否则客户端无法访问此方法
		return $this; //返回自身，表示是一个组合单元
	}
}
class Army extends CompositeUnit{
	function common_method(){
		$set = 0;
		foreach ( $this->units as $unit){
			$set += $unit->common_method();
		}
		return $set;
	}
}

/*
 *用户操作客户端
 */ 
class client{
	/**
	 * dounits()
	 * 组合对象或移除对象
	 * @param  $first 		欲与第二个对象组合，或欲从此对象中移除包含的第二个对象
	 * @param  $second 		欲加入或移除的对象
	 * @param  $contrl 		值为1表示要组合进第一个对象，值为0表示欲移除此对象__halt_compiler
	 * @return  结果对象
	 */
	static function dounits (Unit $first,Unit $second,$contrl=1){
		if(!is_null($first->is_composite())){
			$composite = $first;
		}else{
			$composite=new Army();//如果两个对象都是最小单元，生成一个容器组合对象
			$composite->add_unit($first);
		}
		if ($contrl>0){
			$composite->add_unit($second);
		}else{
			$composite->remove_unit($second);
		}
		return $composite;
	}
}
$archer = new Archer();
$tank = new Tank();
$army = client::dounits($archer,$tank);//通过客户端组合两个对象到为一个集合
echo $army->common_method(); //输出集合的战斗力
?>
```
